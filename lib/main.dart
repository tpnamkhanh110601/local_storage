import 'package:flutter/material.dart';
import 'package:local_storage/home.dart';
import 'package:get/get.dart';
import 'package:local_storage/handleFile.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const GetMaterialApp(
      home: Scaffold(
        body: HomePage(),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController _strController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextField(
            controller: _strController,
            decoration: const InputDecoration(labelText: 'Data'),
          ),
          ElevatedButton(
              onPressed: () {
                writeData(_strController.text);
                return navigateToSecondPage();
              },
              child: Text('Submit'))
        ],
      ),
    );
  }
}

void navigateToSecondPage() {
  Get.to(const SecondPage());
}
