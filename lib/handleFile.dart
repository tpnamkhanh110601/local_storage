import 'dart:io';
import 'package:path_provider/path_provider.dart';

Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();
  return directory.path;
}

Future<File> get _localFile async {
  final path = await _localPath;
  return File('$path/data.txt');
}

void writeData(String param) async {
  final file = await _localFile;
  file.writeAsString(param);
}

Future<String> readData() async {
  try {
    final file = await _localFile;
    final contents = await file.readAsString();

    return contents;
  } catch (e) {
    return '';
  }
}
